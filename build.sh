#!/bin/bash

if [[ -z "${1}" ]]; then
    echo "No device specified."
    exit
fi

export TARGET_BOARD_PLATFORM="${1}"
export TARGET_BUILD_VARIANT=user

export ANDROID_BUILD_TOP=$(pwd)
export ANDROID_PRODUCT_OUT=${ANDROID_BUILD_TOP}/out/target/product/${TARGET_BOARD_PLATFORM}
export OUT_DIR=${ANDROID_BUILD_TOP}/out/msm-kernel-${TARGET_BOARD_PLATFORM}

# for Audio(techpack) driver build
export MODNAME=vendor_dlkm

export EXT_MODULES="
	../sm6225-modules/qcom/opensource/mmrm-driver
	../sm6225-modules/qcom/opensource/mm-drivers/hw_fence
	../sm6225-modules/qcom/opensource/mm-drivers/msm_ext_display
	../sm6225-modules/qcom/opensource/mm-drivers/sync_fence
	../sm6225-modules/qcom/opensource/audio-kernel
	../sm6225-modules/qcom/opensource/camera-kernel
	../sm6225-modules/qcom/opensource/dataipa/drivers/platform/msm
	../sm6225-modules/qcom/opensource/datarmnet/core
	../sm6225-modules/qcom/opensource/datarmnet-ext/aps
	../sm6225-modules/qcom/opensource/datarmnet-ext/offload
	../sm6225-modules/qcom/opensource/datarmnet-ext/shs
	../sm6225-modules/qcom/opensource/datarmnet-ext/perf
	../sm6225-modules/qcom/opensource/datarmnet-ext/perf_tether
	../sm6225-modules/qcom/opensource/datarmnet-ext/sch
	../sm6225-modules/qcom/opensource/datarmnet-ext/wlan
	../sm6225-modules/qcom/opensource/securemsm-kernel
	../sm6225-modules/qcom/opensource/display-drivers/msm
	../sm6225-modules/qcom/opensource/video-driver
	../sm6225-modules/qcom/opensource/graphics-kernel
	../sm6225-modules/qcom/opensource/touch-drivers
	../sm6225-modules/qcom/opensource/wlan/platform
	../sm6225-modules/qcom/opensource/wlan/qcacld-3.0
	../sm6225-modules/qcom/opensource/bt-kernel
"

export LTO=thin

RECOMPILE_KERNEL=1 ./kernel_platform/build/android/prepare_vendor.sh ${TARGET_BOARD_PLATFORM} gki \
2>&1 | tee kernel.log
